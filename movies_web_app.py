#!/usr/bin/env python3

import os
import time

from flask import request
from flask import Flask, render_template
import mysql.connector
from mysql.connector import errorcode


application = Flask(__name__)
app = application


def get_db_creds():
    db = os.environ.get("DB", None) or os.environ.get("database", None)
    username = os.environ.get("USER", None) or os.environ.get("username", None)
    password = os.environ.get("PASSWORD", None) or os.environ.get("password", None)
    hostname = os.environ.get("HOST", None) or os.environ.get("dbhost", None)
    return db, username, password, hostname


def create_table():
    # Check if table exists or not. Create and populate it only if it does not exist.
    db, username, password, hostname = get_db_creds()
    table_ddl = 'CREATE TABLE movies_table1(id INT UNSIGNED NOT NULL AUTO_INCREMENT, Year varchar(255), Title varchar(255), Director varchar(255), Actor varchar(255), Release_date varchar(255), Rating DOUBLE, PRIMARY KEY (id))'

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        #try:
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)
        #except Exception as exp1:
        #    print(exp1)

    cur = cnx.cursor()

    try:
        cur.execute(table_ddl)
        cnx.commit()
        populate_data()
    except mysql.connector.Error as err:
        if err.errno == errorcode.ER_TABLE_EXISTS_ERROR:
            print("already exists.")
        else:
            print(err.msg)


def populate_data():

    db, username, password, hostname = get_db_creds()

    print("Inside populate_data")
    print("DB: %s" % db)
    print("Username: %s" % username)
    print("Password: %s" % password)
    print("Hostname: %s" % hostname)

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                       host=hostname,
                                       database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)

    cur = cnx.cursor()
    cur.execute("INSERT INTO message (greeting) values ('Hello, World!')")
    cnx.commit()
    print("Returning from populate_data")


def query_data():

    db, username, password, hostname = get_db_creds()

    print("Inside query_data")
    print("DB: %s" % db)
    print("Username: %s" % username)
    print("Password: %s" % password)
    print("Hostname: %s" % hostname)

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)

    cur = cnx.cursor()

    cur.execute("SELECT greeting FROM message")
    entries = [dict(greeting=row[0]) for row in cur.fetchall()]
    return entries

try:
    print("---------" + time.strftime('%a %H:%M:%S'))
    print("Before create_table global")
    create_table()
    print("After create_data global")
except Exception as exp:
    print("Got exception %s" % exp)
    conn = None


@app.route('/add_movie', methods=['POST'])
def add_movie():
    result = request.form

    year = result['year']
    title = result['title']
    director = result['director']
    actor = result['actor']
    release_date = result['release_date']
    rating = result['rating']

    db, username, password, hostname = get_db_creds()

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)
    cur = cnx.cursor()
    sql = "SELECT * from movies_table1 WHERE Title=%s"
    val = (title,)
    cur.execute(sql, val)
    results = cur.fetchall()

    if results:
        return movie_insert_failure(title)

    cur = cnx.cursor()
    sql = "INSERT INTO movies_table1 (year, title, director, actor, release_date, rating) VALUES (%s, %s, %s, %s, %s, %s)"
    val = (year, title, director, actor, release_date, rating)
    cur.execute(sql, val)
    cnx.commit()

    return movie_insert_success(title)


@app.route('/update_movie', methods=['POST'])
def update_movie():
    result = request.form

    year = result['year']
    title = result['title']
    director = result['director']
    actor = result['actor']
    release_date = result['release_date']
    rating = result['rating']

    db, username, password, hostname = get_db_creds()

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)

    cur = cnx.cursor()
    sql = "SELECT * from movies_table1 where Title=%s"
    val = (title,)
    cur.execute(sql, val)
    results = cur.fetchall()

    if not results:
        return movie_update_failure(title)

    cur = cnx.cursor()
    sql = "UPDATE movies_table1 SET year=%s, director=%s, actor=%s, release_date=%s, rating=%s WHERE Title=%s"
    val = (year, director, actor, release_date, rating, title)
    cur.execute(sql, val)
    cnx.commit()

    return movie_update_success(title)


@app.route('/delete_movie', methods=['POST'])
def delete_movie():
    result = request.form

    title = result['delete_title']

    db, username, password, hostname = get_db_creds()

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)

    cur = cnx.cursor()
    sql = "SELECT * from movies_table1 WHERE Title=%s"
    val = (title,)
    cur.execute(sql, val)
    results = cur.fetchall()

    if not results:
        return delete_failure(title)
    else:
        cnx = ''
        try:
            cnx = mysql.connector.connect(user=username, password=password,
                                          host=hostname,
                                          database=db)
        except Exception as exp:
            print(exp)
            import MySQLdb
            cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)

        cur = cnx.cursor()
        sql = "DELETE from movies_table1 WHERE Title=%s"
        val = (title,)
        cur.execute(sql, val)
        cnx.commit()

        return movie_delete_success(title)


@app.route('/add_to_db', methods=['POST'])
def add_to_db():
    print("Received request.")
    print(request.form['message'])
    msg = request.form['message']

    db, username, password, hostname = get_db_creds()

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)

    cur = cnx.cursor()
    cur.execute("INSERT INTO message (greeting) values ('" + msg + "')")
    cnx.commit()
    return hello()


@app.route("/search_movie", methods=['GET'])
def search_movie():
    actor = request.args.get('search_actor')

    db, username, password, hostname = get_db_creds()

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)

    cur = cnx.cursor()
    sql = "SELECT Title, Year, Actor from movies_table1 WHERE actor=%s"
    val = (actor,)
    cur.execute(sql, val)

    results = cur.fetchall()
    search_results_list = []

    if results:
        for r in results:
            search_results_list.append({
                'Title': r[0],
                'Year': r[1],
                'Actor': r[2]
            })
        return search_success(search_results_list)
    else:
        return search_failure(actor)


@app.route("/highest_rating", methods=['GET'])
def search_highest_rating():
    db, username, password, hostname = get_db_creds()

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)

    cur = cnx.cursor()
    cur.execute("SELECT * from movies_table1 ORDER BY Rating DESC")

    highest_rating = cur.fetchone()[6]

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)

    cur = cnx.cursor()
    sql = "SELECT Title, Year, Actor, Director, Rating from movies_table1 WHERE Rating=%s"
    val = (highest_rating,)
    cur.execute(sql, val)

    results = cur.fetchall()
    results_list = []

    if results:
        for r in results:
            results_list.append({
                'Title': r[0],
                'Year': r[1],
                'Actor': r[2],
                'Director': r[3],
                'Rating': r[4]
            })
        return movie_by_rating(results_list)
    else:
        return no_movies_in_db()


@app.route("/lowest_rating", methods=['GET'])
def search_lowest_rating():
    db, username, password, hostname = get_db_creds()

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)

    cur = cnx.cursor()
    cur.execute("SELECT * from movies_table1 ORDER BY Rating ASC")

    lowest_rating = cur.fetchone()[6]

    cnx = ''
    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)

    cur = cnx.cursor()
    sql = "SELECT Title, Year, Actor, Director, Rating from movies_table1 WHERE Rating=%s"
    val = (lowest_rating,)
    cur.execute(sql, val)

    results = cur.fetchall()
    results_list = []

    if results:
        for r in results:
            results_list.append({
                'Title': r[0],
                'Year': r[1],
                'Actor': r[2],
                'Director': r[3],
                'Rating': r[4]
            })
        return movie_by_rating(results_list)
    else:
        return no_movies_in_db()

@app.route("/")
def hello():
    print("Inside hello")
    print("Printing available environment variables")
    print(os.environ)
    print("Before displaying index.html")
    entries = query_data()
    print("Entries: %s" % entries)
    return render_template('index.html', entries=entries)


@app.route("/")
def movie_insert_success(title):
    message = "Movie <" + title + "> inserted successfully."
    return render_template('index.html', message=message)


@app.route("/")
def movie_insert_failure(title):
    message = "Movie <" + title + "> could not be inserted becuase it already exists."
    return render_template('index.html', message=message)


@app.route("/")
def movie_update_success(title):
    message = "Movie <" + title + "> updated successfully."
    return render_template('index.html', message=message)


@app.route("/")
def movie_update_failure(title):
    message = "Movie <" + title + "> could not be updated since it does not yet exist."
    return render_template('index.html', message = message)


@app.route("/")
def delete_failure(title):
    message = "Movie with title <" + title + "> does not exist."
    return render_template('index.html', message=message)


@app.route("/")
def movie_delete_success(title):
    message = "Movie <" + title + "> deleted successfully."
    return render_template('index.html', message=message)


@app.route("/")
def search_success(search_results_list):
    return render_template('index.html', list1=search_results_list)


@app.route("/")
def search_failure(actor):
    message = "No movie found for actor <" + actor + ">."
    return render_template('index.html', message=message)


@app.route("/")
def movie_by_rating(results_list):
    return render_template('index.html', list2=results_list)


@app.route("/")
def no_movies_in_db():
    message = "No movies found in database."
    return render_template('index.html', message=message)


if __name__ == "__main__":
    app.debug = True
    app.run(host='0.0.0.0')
